package jacksontest;

public class Trips {

	
String stationStart;
	
	/** The station end. */
	String stationEnd;
	
	/** The started journey at. */
	long startedJourneyAt;
	
	/** The cost in cents. */
	long costInCents;
	
	/** The zone from. */
	long zoneFrom;
	
	/** The zone to. */
	long zoneTo;

	/**
	 * @return the stationStart
	 */
	public String getStationStart() {
		return stationStart;
	}

	/**
	 * @param stationStart the stationStart to set
	 */
	public void setStationStart(String stationStart) {
		this.stationStart = stationStart;
	}

	/**
	 * @return the stationEnd
	 */
	public String getStationEnd() {
		return stationEnd;
	}

	/**
	 * @param stationEnd the stationEnd to set
	 */
	public void setStationEnd(String stationEnd) {
		this.stationEnd = stationEnd;
	}

	/**
	 * @return the startedJourneyAt
	 */
	public long getStartedJourneyAt() {
		return startedJourneyAt;
	}

	/**
	 * @param startedJourneyAt the startedJourneyAt to set
	 */
	public void setStartedJourneyAt(long startedJourneyAt) {
		this.startedJourneyAt = startedJourneyAt;
	}

	/**
	 * @return the costInCents
	 */
	public long getCostInCents() {
		return costInCents;
	}

	/**
	 * @param costInCents the costInCents to set
	 */
	public void setCostInCents(long costInCents) {
		this.costInCents = costInCents;
	}

	/**
	 * @return the zoneFrom
	 */
	public long getZoneFrom() {
		return zoneFrom;
	}

	/**
	 * @param zoneFrom the zoneFrom to set
	 */
	public void setZoneFrom(long zoneFrom) {
		this.zoneFrom = zoneFrom;
	}

	/**
	 * @return the zoneTo
	 */
	public long getZoneTo() {
		return zoneTo;
	}

	/**
	 * @param zoneTo the zoneTo to set
	 */
	public void setZoneTo(long zoneTo) {
		this.zoneTo = zoneTo;
	}
	
	
}

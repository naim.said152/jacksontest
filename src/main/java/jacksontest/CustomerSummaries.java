package jacksontest;

import java.util.ArrayList;
import java.util.List;

public class CustomerSummaries {
	
	private List<CustomerOutput> listCustomerOutputs = new ArrayList<>();

	/**
	 * @return the listCustomerOutputs
	 */
	public List<CustomerOutput> getListCustomerOutputs() {
		return listCustomerOutputs;
	}

	/**
	 * @param listCustomerOutputs the listCustomerOutputs to set
	 */
	public void setListCustomerOutputs(List<CustomerOutput> listCustomerOutputs) {
		this.listCustomerOutputs = listCustomerOutputs;
	}
	
	
	

}

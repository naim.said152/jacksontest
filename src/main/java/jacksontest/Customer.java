package jacksontest;

// TODO: Auto-generated Javadoc
/**
 * The Class Customer.
 */
public class Customer {
	
	/** The unix timestamp. */
	private long unixTimestamp;
	
	/** The customer id. */
	private long customerId;
    
    /** The station. */
    private String station;
	
	/**
	 * Gets the unix timestamp.
	 *
	 * @return the unixTimestamp
	 */
	public long getUnixTimestamp() {
		return unixTimestamp;
	}
	
	/**
	 * Sets the unix timestamp.
	 *
	 * @param unixTimestamp the unixTimestamp to set
	 */
	public void setUnixTimestamp(long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
	}
	
	/**
	 * Gets the customer id.
	 *
	 * @return the customerId
	 */
	public long getCustomerId() {
		return customerId;
	}
	
	/**
	 * Sets the customer id.
	 *
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	
	/**
	 * Gets the station.
	 *
	 * @return the station
	 */
	public String getStation() {
		return station;
	}
	
	/**
	 * Sets the station.
	 *
	 * @param station the station to set
	 */
	public void setStation(String station) {
		this.station = station;
	}
	
	/**
	 * Instantiates a new customer.
	 */
	public Customer() {
		
	}
	
	/**
	 * Instantiates a new customer.
	 *
	 * @param unixTimestamp the unix timestamp
	 * @param customerId the customer id
	 * @param station the station
	 */
	public Customer(long unixTimestamp, long customerId, String station) {
		super();
		this.unixTimestamp = unixTimestamp;
		this.customerId = customerId;
		this.station = station;
	}

    
    
    
    
    
}

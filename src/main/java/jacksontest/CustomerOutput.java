package jacksontest;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerOutput.
 */
public class CustomerOutput {

	/** The customer id. */
	long customerId;
	
	/** The total cost in cents. */
	long totalCostInCents;
	
	Trips trips;

	/**
	 * @return the customerId
	 */
	public long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the totalCostInCents
	 */
	public long getTotalCostInCents() {
		return totalCostInCents;
	}

	/**
	 * @param totalCostInCents the totalCostInCents to set
	 */
	public void setTotalCostInCents(long totalCostInCents) {
		this.totalCostInCents = totalCostInCents;
	}

	/**
	 * @return the trips
	 */
	public Trips getTrips() {
		return trips;
	}

	/**
	 * @param trips the trips to set
	 */
	public void setTrips(Trips trips) {
		this.trips = trips;
	}

	/** The station start. */
	
	
	

}

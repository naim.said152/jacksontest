package jacksontest;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

/**
 * The Class JsonParse.
 */
public class JsonParse {

	final static Logger logger = Logger.getLogger(JsonParse.class);

	/**
	 * Instantiates a new json parse.
	 */
	public JsonParse() {
	}

	/**
	 * Gets the customers.
	 *
	 * @return the customers
	 */
	public static List<CustomerOutput> getCustomers() {

		List<CustomerOutput> listCustomerOutputs = new ArrayList<>();
		JSONParser jsonParser = new JSONParser();
		CustomerSummaries customerSummaries = new CustomerSummaries();

		FileReader reader;
		Object obj = null;
		try {
			reader = new FileReader("customers.json");

			// Read JSON file
			obj = jsonParser.parse(reader);

		} catch (IOException | ParseException e1) {

			logger.error(e1.getMessage());
		}

		// A JSON object. Key value pairs are unordered. JSONObject supports
		// java.util.Map interface.
		JSONObject jsonObject = (JSONObject) obj;

		// A JSON array. JSONObject supports java.util.List interface.
		JSONArray customersJsonArray = (JSONArray) jsonObject.get("taps");
		String customersJson = customersJsonArray.toString();

		ObjectMapper mapper = new ObjectMapper();

		try {

			// convert JSON array to List of objects
			List<Customer> listCustomersFromJson = Arrays.asList(mapper.readValue(customersJson, Customer[].class));

			List<List<Customer>> listCustomer = new ArrayList<List<Customer>>();

			List<Customer> sortedList = listCustomersFromJson.stream()
					.sorted(Comparator.comparingLong(Customer::getCustomerId).thenComparing(Customer::getUnixTimestamp))
					.collect(Collectors.toList());

			for (int j = 0; j < sortedList.size() - 1; ++j) {
				if (j % 2 != 0)
					continue;

				List<Customer> list = new ArrayList<Customer>();
				Customer customer1 = sortedList.get(j);
				Customer customer2 = sortedList.get(j + 1);

				list.add(customer1);
				list.add(customer2);
				listCustomer.add(list);

			}

			boolean paire = false;

			for (List<Customer> customers : listCustomer) {

				CustomerOutput customerOutput = new CustomerOutput();
				Trips trips = new Trips();

				for (Customer customer : customers) {

					if (paire == false) {
						customerOutput.setCustomerId(customer.getCustomerId());
						trips.setStationStart(customer.getStation());
						trips.setZoneFrom(customer.getUnixTimestamp());

						if (customer.getStation().equalsIgnoreCase("A") || customer.getStation().equalsIgnoreCase("B")
								|| customer.getStation().equalsIgnoreCase("C")) {
							trips.setZoneFrom(1);
							customerOutput.setTrips(trips);
						} else if (customer.getStation().equalsIgnoreCase("D")
								|| customer.getStation().equalsIgnoreCase("E")) {
							trips.setZoneFrom(2);
							customerOutput.setTrips(trips);
						} else if (customer.getStation().equalsIgnoreCase("F")
								|| customer.getStation().equalsIgnoreCase("E")) {
							trips.setZoneFrom(3);
							customerOutput.setTrips(trips);
						} else if (customer.getStation().equalsIgnoreCase("G")
								|| customer.getStation().equalsIgnoreCase("H")) {
							trips.setZoneFrom(4);
							customerOutput.setTrips(trips);
						}
					}

					if (paire == true) {
						trips.setStationEnd(customer.getStation());
						trips.setZoneTo(customer.getUnixTimestamp());
						trips.setStartedJourneyAt(customer.getUnixTimestamp());

						if (customer.getStation().equalsIgnoreCase("A") || customer.getStation().equalsIgnoreCase("B")
								|| customer.getStation().equalsIgnoreCase("C")) {
							trips.setZoneTo(1);
							customerOutput.setTrips(trips);
						} else if (customer.getStation().equalsIgnoreCase("D")
								|| customer.getStation().equalsIgnoreCase("E")) {
							trips.setZoneTo(2);
							customerOutput.setTrips(trips);
						} else if (customer.getStation().equalsIgnoreCase("F")
								|| customer.getStation().equalsIgnoreCase("E")) {
							trips.setZoneTo(3);
							customerOutput.setTrips(trips);
						} else if (customer.getStation().equalsIgnoreCase("G")
								|| customer.getStation().equalsIgnoreCase("H")) {
							trips.setZoneTo(4);
							customerOutput.setTrips(trips);
						}
						listCustomerOutputs.add(customerOutput);

					}

					paire = !paire;

				}

			}

		} catch (IOException e) {
			logger.error(e.getMessage());
		}

		for (CustomerOutput customerOutput : listCustomerOutputs) {
			if ((customerOutput.getTrips().getZoneFrom() == 1 && customerOutput.getTrips().getZoneTo() == 2)
					|| (customerOutput.getTrips().getZoneFrom() == 2 && customerOutput.getTrips().getZoneTo() == 1)) {
				customerOutput.getTrips().setCostInCents(240);
			}

			else if ((customerOutput.getTrips().getZoneFrom() == 3 && customerOutput.getTrips().getZoneTo() == 4)
					|| (customerOutput.getTrips().getZoneFrom() == 4 && customerOutput.getTrips().getZoneTo() == 3)) {
				customerOutput.getTrips().setCostInCents(200);
			} else if ((customerOutput.getTrips().getZoneFrom() == 3 && customerOutput.getTrips().getZoneTo() == 1)) {
				customerOutput.getTrips().setCostInCents(280);
			} else if ((customerOutput.getTrips().getZoneFrom() == 4 && customerOutput.getTrips().getZoneTo() == 1)) {
				customerOutput.getTrips().setCostInCents(300);
			} else if (((customerOutput.getTrips().getZoneFrom() == 1 || customerOutput.getTrips().getZoneFrom() == 2)
					&& customerOutput.getTrips().getZoneTo() == 3)) {
				customerOutput.getTrips().setCostInCents(280);
			} else if (((customerOutput.getTrips().getZoneFrom() == 1 || customerOutput.getTrips().getZoneFrom() == 2)
					&& customerOutput.getTrips().getZoneTo() == 4)) {
				customerOutput.getTrips().setCostInCents(300);
			}

		}

		customerSummaries.setListCustomerOutputs(listCustomerOutputs);
		ObjectMapper mapper2 = new ObjectMapper();

		String json = new Gson().toJson(customerSummaries);
		// Object to JSON in file
		try {
			mapper2.writeValue(new File("customersOutput.json"), json);
		} catch (JsonGenerationException e) {
			logger.error(e.getMessage());

		} catch (JsonMappingException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return listCustomerOutputs;

	}

}

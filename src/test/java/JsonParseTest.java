

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import jacksontest.CustomerOutput;
import jacksontest.JsonParse;



public class JsonParseTest {

    @Test
    public void shouldParseStrings() {
        
       List<CustomerOutput> listCustomerOutputs = JsonParse.getCustomers();
       
       CustomerOutput customerOutput =listCustomerOutputs.get(0);
       
       
       assertEquals(customerOutput.getCustomerId(), 1);
       assertEquals(customerOutput.getTotalCostInCents(), 0);
       assertEquals(customerOutput.getTrips().getStationStart(), "A");
       assertEquals(customerOutput.getTrips().getStationEnd(), "D");
       assertEquals(customerOutput.getTrips().getStartedJourneyAt(), 2);
       assertEquals(customerOutput.getTrips().getCostInCents(), 240);
       assertEquals(customerOutput.getTrips().getZoneFrom(), 1);
       
       
      
    }

   
    
}